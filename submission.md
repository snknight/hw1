#### My name is Shelby Knight

This is my submission for homework1.
I would like to include one link: [Twitch](https://www.twitch.tv/)

I would also like to include some facts in a table:

|   Item        |         Info               |
|---------------|----------------------------|
| home town     |      Houston, TX           |
| favorite song | Mastermind by Deltron 3030 |
